import {useEffect, useState} from "react";
import {columnsUsersTable} from "./UsersData";
import {User} from "./userModel";



export function useUsers(){
    const [users, setUsers] = useState<User[]>([])
    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(res => res.json())
            .then(res => setUsers(res))
    }, [])
    const columns = columnsUsersTable;
    const removeUser = (id: number) => {
        fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {method: 'DELETE'})
            .then(res => res.json())
            .then(res => setUsers(users.filter(ele => ele.id !== id)))
    }
    return {
     columns,
     users,
     removeUser
    }
}
