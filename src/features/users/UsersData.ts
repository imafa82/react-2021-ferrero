import {ColumnsTable} from "../../shared/design/table/models/ColumnsTable";

export const columnsUsersTable: ColumnsTable[] = [
    {
        label: 'Nome',
        field: 'name'
    },
    {
        label: 'Username',
        field: 'username'
    },
    {
        label: 'Email',
        field: 'email'
    },
    {
        label: '',
        field: 'actions'
    }
]
