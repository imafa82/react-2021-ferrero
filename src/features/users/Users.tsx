import React, {useEffect, useState} from 'react';
import TableCustom from "../../shared/design/table/TableCustom";
import {ColumnsTable} from "../../shared/design/table/models/ColumnsTable";
import {classListTableData} from "../../shared/design/table/models/TableModel";
import {useSearch} from "../../shared/utils/useSearch";
import {columnsUsersTable} from "./UsersData";
import {useUsers} from "./useUsers";



const Users: React.FC = () => {
    const {users, columns, removeUser}= useUsers()
    const {search, setSearch, filterData} = useSearch(users)
    const templates = {
        actions: (value: any, row: any) => {
            return <i className="fa fa-trash" onClick={() => removeUser(row.id)}></i>
        }
    }
    return(
        <>
            <input value={search} onChange={(event) => setSearch(event.target.value)} />
            <TableCustom classList={[classListTableData.alternativeRow]} columns={columns} data={filterData} templates={templates}/>
        </>
    )
}

export default Users;
