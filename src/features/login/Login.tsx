import React, {FormEvent, useState} from "react";
import Button from "../../shared/design/button/Button";
import {useHistory} from "react-router-dom"
import http from "../../shared/utils/http";

interface UserLogin{
    email: string;
    password: string;
}

interface User{
    email: string;
    name: string;
    surname: string;
    _id: string;
}

const Login: React.FC<{loginAction: () => void}> = ({loginAction}) => {
    const history = useHistory()
    const [userData, setUserData] = useState<Partial<UserLogin>>({});
    const login = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        http.post<{token: string, user: User}>(`login`, userData, {headers: {
                'Content-Type': 'application/json'
            }}).then(res => {
            localStorage.setItem('token', res.token)
            loginAction && loginAction();
            history.push('/users')
        })
        // console.log(userData)
    }
    const changeData = (event: React.ChangeEvent<HTMLInputElement>) => {
        setUserData({...userData, [event.target.name] : event.target.value})
    }
    return (
        <div>
            <h1>Login</h1>
            <form onSubmit={login}>
                <div className="form-group">
                    <input type="email" className="form-control" name="email" onChange={changeData} value={userData.email}/>
                </div>
                <div className="form-group">
                    <input type="password" className="form-control" name="password" onChange={changeData} value={userData.password}/>
                </div>
                <Button type="submit">Login</Button>
            </form>
        </div>
    )
}

export default Login
