import Auth from "../core/Auth";

const status401Interceptor = (err: any) => {
    if(err.response && err.response.status === 401){
        console.log('processo di logout')
        Auth.logout();
    }
    return Promise.reject(err);
}
export default status401Interceptor;
