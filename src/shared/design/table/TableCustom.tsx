import React from 'react';
import {ColumnsTable} from "./models/ColumnsTable";
import {classListTable} from "./models/TableModel";

interface TableCustomProps{
    data?: {[key: string]: any};
    columns: ColumnsTable[],
    primaryKey?: string;
    classList?: classListTable; // ('alternating-row' | 'test')[]
    templates?: {[key:string]: (value: string, row: any) => React.ReactNode}
}

const TableCustom: React.FC<TableCustomProps> = ({
    data = [],
    columns =[],
    primaryKey= 'id',
    classList = [],
    templates
}) => {
    const classData = ['table-custom', ...classList]
    return(
        <table className={classData.join(' ')}>
            <thead>
            <tr>
                {columns.map(ele => <th key={ele.field}>{ele.label}</th>)}
            </tr>
            </thead>
            <tbody>
            {
                data.map((ele: any, index: number) => <tr key={primaryKey? ele[primaryKey] : index}>
                    {columns.map(d => <td key={d.field}>
                        {templates && templates[d.field]?
                            templates[d.field](ele[d.field], ele)
                            : ele[d.field]}
                    </td>)}
                </tr>)
            }
            </tbody>

        </table>
    )
}

export default TableCustom;
