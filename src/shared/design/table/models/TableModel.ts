import {ValueOf} from "../../../utils/typescript/ValueOf";

export const classListTableData = {
    alternativeRow: 'alternating-row',
    test: 'test'
} as const;

export type classListTable = ValueOf<typeof classListTableData>[];


// type classListTableDataType = typeof classListTableData;
// type classTest = {
//     readonly alternativeRow: 'alternating-row',
//     readonly test: 'test'
// }

// type classListTableDataTypes = keyof classListTableDataType;
// export type classListTableDataTypes = 'alternativeRow' | 'test';

// export type classListTable = (classListTableDataType[classListTableDataTypes])[];

// export type classListTable = ('alternating-row' | 'test')[];
