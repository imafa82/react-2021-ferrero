import React from 'react'
import css from './Button.module.scss'

function Button(props: any){
    let type: 'button' | 'submit' | 'reset' = props.type || 'button';
    let classList = [css.btn, (props.classBt || css.btnPrimary)]
    let text = props.text || 'Salva';
    return(
        <button
            onClick={props.clickAction}
            type={type}
            className={classList.join(' ')}>{props.children || text}</button>
    )
}

export default Button;
