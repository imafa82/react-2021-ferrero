import React from 'react';
import {BootstrapType} from "./module/BootstrapType";

interface CardCustomProps{
    title: string;
    classList?: string[];
    headerClass?: BootstrapType[];
    bodyClass?: BootstrapType[];
}

const CardCustom: React.FC<CardCustomProps> = ({
   title,
   children,
   classList = [],
    bodyClass = [],
    headerClass = []
}) => {
    const listClass = ['card', ...classList]
    const listHeader = ['card-header', ...headerClass]
    const listBody = ['card-body', ...bodyClass]
    return(
        <div className={listClass?.join(' ')}>
            <div className={listHeader.join(' ')}>
                {title}
            </div>
            <div className={listBody.join(' ')}>
                {children}
            </div>
        </div>
    )
}

export default CardCustom;
