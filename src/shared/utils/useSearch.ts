import {useState} from "react";

export function useSearch(data: any[], properties: string[] = []){
    const [search, setSearch] = useState<string>('')

    const filterData = data.filter(ele =>
            (properties.length ? properties : Object.keys(ele))
                .reduce((acc, property) =>
                        ele[property]?.toString()?.trim().toLowerCase().includes(search.trim().toLowerCase()) || acc, false)
    )
    return {
        search,
        setSearch,
        filterData
    }
}
