import React, {useEffect, useState} from 'react';
import './App.scss';
import {BrowserRouter as Router, Route, Redirect} from 'react-router-dom'
import AuthRoute from "./core/routes/AuthRoute";
import PublicRoute from "./core/routes/PublicRoute";
import http from "./shared/utils/http";
import Auth from "./core/Auth";

function App (){
    const [auth, setAuth] = useState<boolean | undefined >();
    const [user, setUser] = useState<any>();
    const autologin = () => {
        setAuth(true)
        http.get('users/logged').then(res => {
            setUser(res);
        })
    }
    const logout = () => {
        setAuth(false);
        localStorage.removeItem('token');
    }
    useEffect(() => {
        Auth.logout = logout;
        localStorage.getItem('token') ? autologin() : setAuth(false);
    }, [])


      return (
          <Router>
              {
                  auth === undefined || (auth && !user) ?
                      <></> :
                      <>{auth ? <AuthRoute logout={logout} /> : <PublicRoute loginAction={() => setAuth(true)} /> }</>
              }
          </Router>
      );
}

export default App;
