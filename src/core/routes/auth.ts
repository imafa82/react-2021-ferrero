import {Routes} from "./models/Routes";
import Users from "../../features/users/Users";
import Actors from "../../features/actors/Actors";

export const auth: Routes = {
    list: [
        {
            path: '/users',
            name: 'users.index',
            component: Users
        },
        {
            path: '/actors',
            name: 'actors.index',
            component: Actors
        }
    ],
    redirect: '/users'
}
