import React from 'react';
import Users from "../../features/users/Users";
import {Redirect, Route, Switch} from "react-router-dom";
import Header from "../Header";
import {auth} from "./auth";


const AuthRoute = ({logout}: {logout: () => void}) => {

      return (
         <>
             <Header logout={logout} />
             <Switch>
                 {auth.list.map(route => <Route exact path={route.path} component={route.component} key={route.path}/>)}
                 {auth.redirect && <Redirect from="*" to={auth.redirect} />}
             </Switch>

         </>
      );
}

export default AuthRoute;
