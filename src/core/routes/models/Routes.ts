import React from "react";

interface Route{
    path: string;
    component: React.ComponentType<any>;
    name: string;
    permissions?: string[]
}
export interface Routes{
    list: Route[];
    redirect: string;
}
