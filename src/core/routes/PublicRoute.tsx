import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Login from "../../features/login/Login";


const PublicRoute = ({loginAction}: {loginAction: () => void}) => {
      return (
         <>
             <Switch>
                 <Route exact path='/'>
                     <Login loginAction={loginAction} />
                 </Route>
                 <Redirect from="*" to="/" />
             </Switch>
         </>
      );
}

export default PublicRoute;
